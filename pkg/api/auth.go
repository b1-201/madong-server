package api

import (
	"bytes"
	"crypto/ed25519"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"io"
	"strings"
	"time"
)

const AuthType = "MDG-TOKEN-ED25519"

type AuthToken struct {
	Publickey string `json:"idt"` // Base64 encoded 32 byte publickey
	Timestamp uint32 `json:"tsp"` // Unix timestamp padded to minutes
	Nonce     uint32 `json:"nnc"` // Random 32 bit nonce
}

var ErrInvalidToken = errors.New("Invalid token")

func (at *AuthToken) Sign(privkey ed25519.PrivateKey) string {
	var buff bytes.Buffer

	b64enc := base64.NewEncoder(base64.StdEncoding, &buff)
	json.NewEncoder(b64enc).Encode(at)

	// Ehm this seems to flush it. TODO may break stuff below
	b64enc.Close()

	// Sign the current buffer
	signature := ed25519.Sign(privkey, buff.Bytes())

	// Append dot and signature and return result
	buff.Write([]byte{'.'})
	b64enc.Write(signature)

	b64enc.Close()

	return buff.String()
}

// Takes a whole token and decodes the AuthToken block
// DOES NOT VERIFY IT. Use the verifytoken function
func DecodeToken(token string) (*AuthToken, error) {
	// Extract everything before .
	atstr := strings.SplitN(token, ".", 1)
	if len(atstr) == 0 {
		return nil, ErrInvalidToken
	}

	// Put in string in one end
	str := strings.NewReader(atstr[0])

	// Create a chain str -> base64 -> json -> AuthToken
	b64dec := base64.NewDecoder(base64.StdEncoding, str)

	// Json decode
	at := new(AuthToken)
	err := json.NewDecoder(b64dec).Decode(at)

	return at, err
}

// Verifies a full token
func VerifyToken(pubkey ed25519.PublicKey, token string) (bool, error) {
	// Extract the two parts of the token
	atstr := strings.SplitN(token, ".", 2)
	if len(atstr) < 2 {
		return false, ErrInvalidToken
	}
	sig, err := base64.StdEncoding.DecodeString(atstr[1])
	if err != nil {
		return false, err
	}

	// Verify
	okay := ed25519.Verify(pubkey, []byte(atstr[0]), sig)

	return okay, nil

}

func GenAuthToken(pubkey string, rand io.Reader) (*AuthToken, error) {
	// Create a new token
	at := new(AuthToken)

	at.Publickey = pubkey
	// TODO is 32 bit enough?
	at.Timestamp = uint32(time.Now().Unix())

	// Read out random 32 bit value
	err := binary.Read(rand, binary.LittleEndian, &at.Nonce)
	return at, err
}
