package api

type DataType string

func (d *DataType) UnmarshalJSON(b []byte) error {
	*d = DataType(b)
	return nil
}

func (d DataType) MarshalJSON() ([]byte, error) {
	if d == "" {
		d = "null"
	}
	return []byte(d), nil
}

//Oy
type Links []Link

//Ay
type LinkOverview struct {
	Href   string `json:"href"`
	Method string `json:"method"`
}

//Ay
type Link struct {
	Href   string `json:"href"`
	Method string `json:"method"`
}

//Oy
type LinksOverview []LinkOverview

// This defines the response for all the functions
type Response struct {
	Err           string        `json:"err"`
	Data          DataType      `json:"data"`
	Links         Links         `json:"links,omitempty"`
	LinksOverview LinksOverview `json:"linksOverview, omitempty"`
}

// Database stuff
type Role struct {
	Name string `json:"name"`
}

type Card struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Scanner struct {
	Publickey string `json:"publickey"`
	Zone      uint   `json:"zone"`
}

// httpScannerCrypto things
type HttpCipherOpenRsp struct {
	Sessionid string `json:"session"`
	State     uint   `json:"state"`
}

type HttpCipherReq struct {
	Sessionid string  `json:"session"`
	Feedin    []uint8 `json:"feed"`
	Input     []uint8 `json:"input"`
}

type HttpCipherRsp struct {
	Resp   []uint8 `json:"rsp"`
	Parity []uint8 `json:"parity"`
}

type HttpCipherVerifyReq struct {
	Sessionid string  `json:"session"`
	EncBlk    []uint8 `json:"blk"`
}
