.DEFAULT_GOAL:= single

.PHONY: p clean watch single purge run

p:
	gofmt -s -w .

clean:
	docker-compose down

single:
	make p && docker-compose up -d --build

watch:
	find . -name "*.go" | entr make single

local: p
	cd cmd/madong && go build

purge:
	docker system prune -a

run: local
	cmd/madong/madong
