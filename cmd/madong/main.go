package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/patrickmn/go-cache"
)

// Defined the context for the different http functions
type Madong struct {
	test  string
	db    *sqlx.DB
	mux   *mux.Router
	root  string
	git   string
	cache *cache.Cache
}

func main() {
	md := new(Madong)

	md.test = "Hej med dig"
	md.root = os.Getenv("MADONG_ROOT")
	if md.root == "" {
		md.root = "/"
	}
	md.initmux()

	if err := md.setupDB(); err != nil {
		log.Fatal(err)
	}
	defer md.db.Close()

	if err := md.createScheme(); err != nil {
		log.Fatal(err)
	}
	//Get current git hash
	md.getGit()
	// Create cache
	md.cache = cache.New(2*time.Minute, 10*time.Minute)

	err := http.ListenAndServe(":4444", md.mux)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func (md *Madong) getGit() {
	data, err := ioutil.ReadFile(md.root + ".git/refs/heads/master")
	if err != nil {
		fmt.Println("File reading error", err)
		return

	}
	md.git = strings.TrimSuffix(string(data), "\n")
	fmt.Println("Contents of file:", md.git)

}
