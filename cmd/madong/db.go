package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

const dbStr = "host=db user=postgres password=changeme dbname=postgres sslmode=disable"

// Table definitions for postgresql
const tables = `
CREATE TABLE IF NOT EXISTS scanners(
	publickey TEXT UNIQUE PRIMARY KEY,
	zone      INT
);
CREATE TABLE IF NOT EXISTS users(
	id         TEXT UNIQUE PRIMARY KEY,
	name       TEXT,
	key	       BYTEA,
	secret     TEXT,
	pin	       INT
);
CREATE TABLE IF NOT EXISTS roles(
	id   SERIAL PRIMARY KEY,
	name TEXT UNIQUE
);

-- This defines a many to many link between roles and users.
-- Every row is a pair between a user and a role.
-- The double primary key means that there can not the two rows the same pair (user_id, role_id)
CREATE TABLE IF NOT EXISTS roles_users(
	user_id  TEXT REFERENCES users(id)     ON DELETE CASCADE,
	role_id  INT  REFERENCES roles(id)     ON DELETE CASCADE,
	PRIMARY KEY(user_id, role_id)
);
CREATE TABLE IF NOT EXISTS policies(
	id     SERIAL PRIMARY KEY,
	role   INT REFERENCES roles(id) ON DELETE CASCADE,
	zone   INT,
	tbegin TIME WITHOUT TIME ZONE,
	tend   TIME WITHOUT TIME ZONE,
	state  INT
);
`

// We want to define out own format function on time.Time
// thus we make our own type
type timestamp time.Time

// Here is the format definition, it just prints the time instead of full date.
func (t timestamp) MarshalJSON() ([]byte, error) {
	// This will return the time in the format '"%H:%M"'
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format("15:04"))
	return []byte(stamp), nil
}

func (t *timestamp) UnmarshalJSON(data []byte) error {
	fmt.Printf("Running %s\n", data)
	parsedtime, err := time.Parse("\"15:04\"", string(data))
	if err != nil {
		return err
	}
	*t = timestamp(parsedtime)
	return nil
}

type Policy struct {
	Id    uint      `json:"id"`
	Zone  uint      `json:"zone"`
	Begin timestamp `db:"tbegin",json:"begin"`
	End   timestamp `db:"tend",json:"end"`
	State uint      `json:"state"` // TODO make this a enum
}

// Sets up the database using the dbStr defined above.
func (md *Madong) setupDB() error {
	var err error

	md.db, err = sqlx.Connect("postgres", dbStr)
	if err != nil {
		return errors.Wrap(err, "database open failed")
	}

	return nil
}

// Applies the gigantic tables string above
func (md *Madong) createScheme() error {
	log.Printf("Applying %s", tables)
	md.db.MustExec(tables)
	return nil
}

// Run a single sql commands on a list using a single transaction.
// The sql command performed is defined as action which is a closure, which can easily be implemented in the calling function.
func (md *Madong) sqlSelectOnList(list []string, action func(tx *sql.Tx, elem string) error) error {
	// Start the transaction, if an error occurs we can undo everything done
	tx, err := md.db.Begin()
	if err != nil {
		return err
	}

	for _, elem := range list {
		// Perform the user action
		err := action(tx, elem)

		if err != nil {
			// Oh no abort
			if rollbackErr := tx.Rollback(); rollbackErr != nil {
				// rollback failed return both errors combined
				return fmt.Errorf("%s, failed rollback: %s", err, rollbackErr)
			}

			return err
		}
	}
	// Everything worked fine, commit it
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}
