package main

import (
	"bytes"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
	"github.com/patrickmn/go-cache"
	"gitlab.com/b1-201/gofare/crypt"
	"gitlab.com/b1-201/madong-server/pkg/api"
)

type cryptSession struct {
	id    string
	state uint
	ks    crypt.Cipher
}

func (md *Madong) httpScannersNew(w http.ResponseWriter, r *http.Request) {
	var scanner api.Scanner

	// Read user input
	err := json.NewDecoder(r.Body).Decode(&scanner)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	_, err = md.db.NamedExec(`INSERT INTO scanners (publickey, zone) VALUES (:publickey, :zone)`, &scanner)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Set the header to StatusCreated to indicate that a new resource was created
	w.WriteHeader(http.StatusCreated)
}

func (md *Madong) httpScannersGet(w http.ResponseWriter, r *http.Request) {

	zone, ok := mux.Vars(r)["zone"]

	if !ok {
		httpError(w, "Mangler zone argument", http.StatusBadRequest)
		return
	}
	var scanners pq.StringArray
	if err := md.db.Select(&scanners, `SELECT publickey FROM scanners WHERE zone=$1`, zone); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	httpFormatResp(w, &scanners, http.StatusOK)

}

const sqlStateForUser = `
SELECT state FROM policies 
JOIN roles_users ON roles_users.role_id = policies.role
JOIN scanners ON scanners.zone = policies.zone
WHERE scanners.publickey = $1
AND user_id = $2
-- Sort if localtime(time right now) is inside tbegin and tend -- 
AND (localtime, INTERVAL '1 second') OVERLAPS (tbegin, tend)
ORDER BY state DESC LIMIT 1
`

func (md *Madong) scannerStateForUser(id string, scanner string) (uint, error) {
	var state uint

	row := md.db.QueryRow(sqlStateForUser, scanner, id)
	err := row.Scan(&state)
	// Handle no rows error
	if err == sql.ErrNoRows {
		return 0, nil
	}
	// General error
	if err != nil {
		return 0, err
	}

	return state, nil
}

func (md *Madong) httpScannersCryptoOpen(w http.ResponseWriter, r *http.Request, scanner string) {
	// Fetch card
	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}

	fmt.Printf("Opening user %s\n", id)

	// Check if user has access to scanners zone
	state, err := md.scannerStateForUser(id, scanner)
	if err != nil {
		httpError(w, fmt.Sprintf("Error fetching state: %s", err.Error()), http.StatusInternalServerError)
		return
	}
	if state == 0 {
		httpError(w, "user not allowed in zone", http.StatusForbidden)
		return
	}
	// Fetch key for user
	var key []uint8
	row := md.db.QueryRow(`SELECT key FROM users WHERE id = $1`, id)
	if err := row.Scan(&key); err != nil {
		httpError(w, fmt.Sprintf("Error fetching user: %s", err.Error()), http.StatusInternalServerError)
		return
	}
	if len(key) == 0 {
		httpError(w, "Key not set for user", http.StatusForbidden)
		return
	}
	fmt.Printf("Using key %X\n", key)
	session := &cryptSession{id: id, state: state, ks: crypt.NewCrypt(key)}

	// Generate random id
	var buf bytes.Buffer
	bs64 := base64.NewEncoder(base64.StdEncoding, &buf)
	bs64.Close()
	io.CopyN(bs64, rand.Reader, 128)
	sessionID := buf.String()

	log.Printf("session ID: " + sessionID)
	md.cache.Add(sessionID, session, cache.DefaultExpiration)

	// Format response
	rsp := &api.HttpCipherOpenRsp{Sessionid: sessionID, State: state}
	httpFormatResp(w, rsp, http.StatusOK)

}

func (md *Madong) httpScannersCryptoCipher(w http.ResponseWriter, r *http.Request) {
	var (
		input  api.HttpCipherReq
		output api.HttpCipherRsp
	)

	// Interpret input
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		httpError(w, err.Error(), http.StatusBadRequest)
	}

	session, ok := md.cache.Get(input.Sessionid)
	if !ok {
		httpError(w, "Incorrect session id", http.StatusForbidden)
		return
	}
	// Cast from interface{} to correct type
	s, ok := session.(*cryptSession)
	if !ok {
		httpError(w, "Incorrect session id", http.StatusForbidden)
		return
	}

	// The default crypt.CryptState does not return errors
	output.Resp, output.Parity, _ = s.ks.Cipher(input.Feedin, input.Input)

	httpFormatResp(w, output, http.StatusOK)
}

// Copied from libnfc/iso14443-subr.c
func calcCRC(input []uint8) []uint8 {
	var wCrc uint32 = 0x6363

	for _, bt := range input {
		bt = (bt ^ uint8(wCrc&0x00FF))
		bt = (bt ^ (bt << 4))
		wCrc = (wCrc >> 8) ^ (uint32(bt) << 8) ^ (uint32(bt) << 3) ^ (uint32(bt) >> 4)
	}

	return []uint8{uint8(wCrc & 0xFF), uint8((wCrc >> 8) & 0xFF)}
}

// Checks if crccheck is the CRC of input
func verifyCRC(input []uint8, crccheck []uint8) bool {
	crc := calcCRC(input)

	return crc[0] == crccheck[0] && crc[1] == crccheck[1]
}

func (md *Madong) httpScannersCryptoVerify(w http.ResponseWriter, r *http.Request) {
	var input api.HttpCipherVerifyReq

	// Interpret input
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		httpError(w, err.Error(), http.StatusBadRequest)
	}

	session, ok := md.cache.Get(input.Sessionid)
	if !ok {
		httpError(w, "Incorrect session id", http.StatusForbidden)
		return
	}
	// Cast from interface{} to correct type
	s, ok := session.(*cryptSession)
	if !ok {
		httpError(w, "Incorrect session id", http.StatusForbidden)
		return
	}

	if len(input.EncBlk) < 18 {
		httpError(w, "Block not long enough", http.StatusBadRequest)
	}

	// Decrypt input
	data, _, _ := s.ks.Cipher(nil, input.EncBlk)

	// Check crc
	if !verifyCRC(data[:16], data[16:]) {
		httpError(w, "CRC error", http.StatusBadRequest)
	}

	log.Printf("Secret %X\n", data[0:16])

	// Returns ErrMismatchedHashAndPassword if data is wrong.
	// Should not return 500 if this happens.
	if err := md.checkSecretHash(s.id, data[0:16]); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("Succesfully authenticated %s\n", s.id)

	httpFormatResp(w, fmt.Sprintf("Welcome %s", s.id), http.StatusOK)
}
