package main

import (
	"crypto/rand"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
	. "gitlab.com/b1-201/madong-server/pkg/api"
	"golang.org/x/crypto/bcrypt"
)

// Get info about a specific user
func (md *Madong) httpUser(w http.ResponseWriter, r *http.Request) {

	links := Links{
		Link{Href: "/users", Method: "POST"},
		Link{Href: "/users/{id}", Method: "GET"},
		Link{Href: "/users/{id}", Method: "DELETE"},
		Link{Href: "/users/search/{name}", Method: "GET"},
		Link{Href: "/users/{id}/checkpin", Method: "POST"},
		Link{Href: "/users/{id}/updatepin", Method: "PATCH"},
		Link{Href: "/users/{id}/roles", Method: "GET"},
		Link{Href: "/users/{id}/roles", Method: "POST"},
		Link{Href: "/users/{id}/roles", Method: "DELETE"},
	}

	/*
	 */

	// Retrieve the card name from request
	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "Mangler kort argument", http.StatusBadRequest)
		return
	}

	var card Card

	err := md.db.Get(&card, "SELECT id, name FROM users WHERE id = $1", id)
	if err != nil {
		httpError(w, err.Error(), http.StatusNotFound)
		return
	}

	//links.Format(id)

	// Print correctly using json
	httpFormatRespWithLinks(w, &card, http.StatusOK, links)
}

// Get all the users from the database
func (md *Madong) httpUsers(w http.ResponseWriter, r *http.Request) {

	links := Links{
		Link{Href: "/users", Method: "POST"},
		Link{Href: "/users/{id}", Method: "GET"},
		Link{Href: "/users/{id}", Method: "DELETE"},
		Link{Href: "/users/search/{name}", Method: "GET"},
		Link{Href: "/users/{id}/checkpin", Method: "POST"},
		Link{Href: "/users/{id}/updatepin", Method: "PATCH"},
		Link{Href: "/users/{id}/roles", Method: "GET"},
		Link{Href: "/users/{id}/roles", Method: "POST"},
		Link{Href: "/users/{id}/roles", Method: "DELETE"},
	}

	var cards []Card
	navn, ok := mux.Vars(r)["query"]

	if ok {
		err := md.db.Select(&cards, "SELECT id, name FROM users WHERE name LIKE '%'|| $1 ||'%'", navn)
		if err != nil {
			httpError(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	err := md.db.Select(&cards, "SELECT name, id FROM users")
	if err != nil {
		fmt.Printf("Hej %s\n", err)
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	} else {
		err := md.db.Select(&cards, "SELECT name, id FROM users")
		if err != nil {
			httpError(w, err.Error(), http.StatusInternalServerError)
			return
		}

	}
	httpFormatRespWithLinks(w, &cards, http.StatusOK, links)
}

// Create a new user. Send along with a json string with the following values set id and roles
func (md *Madong) httpUsersNew(w http.ResponseWriter, r *http.Request) {

	var card Card

	// Read user input
	err := json.NewDecoder(r.Body).Decode(&card)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	_, err = md.db.NamedExec(`INSERT INTO users (name, id) VALUES (:name, :id)`, &card)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Set the header to StatusCreated to indicate that a new resource was created
	w.WriteHeader(http.StatusCreated)
}

// Delete a user again, does not check if the user already exists
func (md *Madong) httpUsersDel(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["card"]
	if !ok {
		http.Error(w, "Mangler kort argument", http.StatusBadRequest)
		return
	}

	// Delete from database
	_, err := md.db.Exec("DELETE FROM users WHERE id = $1", id)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// Check a pin for at user, takes the pin to check from json input { "pin": 1234 }
func (md *Madong) httpCheckPin(w http.ResponseWriter, r *http.Request) {
	// Defines the input format, just add more fields for more user input.
	// This does not define a type but actually a variable input on the stack.
	var input struct {
		Pin uint `json:"pin"`
	}

	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}

	// Decode request using the defined struct above
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		httpError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Get the pin from db
	var userpin uint
	row := md.db.QueryRow("SELECT pin FROM users WHERE id = $1", id)
	if err := row.Scan(&userpin); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Check the pin, and return true or false
	httpFormatResp(w, userpin == input.Pin, http.StatusOK)
}

// Selects all roles for a specific user $1, using the roles_users as a middle table
const selectUsersRoles = `
SELECT roles.name FROM roles
INNER JOIN roles_users ON roles.id = roles_users.role_id
WHERE roles_users.user_id = $1`

func (md *Madong) httpUsersRoles(w http.ResponseWriter, r *http.Request) {
	links := Links{
		Link{Href: "/roles", Method: "POST"},
		Link{Href: "/roles/{id}", Method: "DELETE"},
	}

	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}

	// Query roles Stringarray is just []string, but build for extracting from db
	var roles pq.StringArray
	if err := md.db.Select(&roles, selectUsersRoles, id); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	httpFormatRespWithLinks(w, &roles, http.StatusOK, links)

}

// Create a new link between a user and role using the middle table.
// This takes a name $2 of the role like 'student'.
// But roles_users saves the role_id and not name.
// Thus we need to extract the name using the nested SELECT
const insertRolesUsers = `
INSERT INTO roles_users(user_id, role_id) VALUES 
(
	$1, 
	(
		SELECT id FROM roles 
		WHERE name = $2
	)
)
`

// Add a list of roles to a user
func (md *Madong) httpUsersAddRole(w http.ResponseWriter, r *http.Request) {
	// Definition of input json
	var input struct {
		Roles []string `json:"roles"`
	}

	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}

	// Load user input into input
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Create a new closure sqlstm, which runs insertRolesUsers
	sqlstm := func(tx *sql.Tx, elem string) error {
		_, err := tx.Exec(insertRolesUsers, id, elem)
		return err
	}

	// Run sqlstm on all the roles to add.
	// This will undo everything if one of the sqlstm fails
	err := md.sqlSelectOnList(input.Roles, sqlstm)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return statusCreated because we created a new resource
	w.WriteHeader(http.StatusCreated)
}

// Delete a role $1 from the user $2.
// USING is the same as join.
const deleteUsersRole = `
DELETE FROM roles_users USING roles 
WHERE roles_users.role_id = roles.id
AND roles.name = $1
AND roles_users.user_id = $2
`

func (md *Madong) httpUsersDelRole(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Roles []string `json:"roles"`
	}
	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Define the closure containing the sqlExec
	sqlstm := func(tx *sql.Tx, elem string) error {
		_, err := tx.Exec(deleteUsersRole, elem, id)
		return err
	}

	err := md.sqlSelectOnList(input.Roles, sqlstm)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// Check a pin for at user, takes the pin to check from json input { "pin": 1234 }
func (md *Madong) httpUpdPin(w http.ResponseWriter, r *http.Request) {
	// Defines the input format, just add more fields for more user input.
	// This does not define a type but actually a variable input on the stack.
	var input struct {
		Pin uint `json:"pin"`
	}

	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}

	// Decode request using the defined struct above
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		httpError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Replace the pin in db WIP
	_, err = md.db.Exec(`UPDATE users SET pin=$2 WHERE id = $1`, id, input.Pin)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (md *Madong) httpUsersGenkey(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}

	// Generate a random key
	key := make([]uint8, 6)
	_, err := rand.Read(key)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
	}

	// Save it to db
	_, err = md.db.Exec(`UPDATE users SET key = $2 WHERE id = $1`, id, key)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the new key
	httpFormatResp(w, key, http.StatusOK)
}

func (md *Madong) httpUsersSecret(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["card"]
	if !ok {
		httpError(w, "mangler kort argument", http.StatusBadRequest)
		return
	}

	// Get a single key from the database
	var key []uint8
	row := md.db.QueryRow(`SELECT key FROM users WHERE id = $1`, id)
	if err := row.Scan(&key); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if len(key) == 0 {
		// Link to genkey call
		httpError(w, "Key not set for user", http.StatusInternalServerError)
		return
	}

	// Return the key
	httpFormatResp(w, key, http.StatusOK)
}

func (md *Madong) checkSecretHash(id string, secret []byte) error {

	var hash string
	// Lookup hash
	row := md.db.QueryRow("SELECT secret FROM users WHERE id = $1", id)
	if err := row.Scan(&hash); err != nil {
		return err
	}

	// Compare
	return bcrypt.CompareHashAndPassword([]byte(hash), secret)

}
