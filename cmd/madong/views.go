package main

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/patrickmn/go-cache"
	"gitlab.com/b1-201/madong-server/pkg/api"
)

func (md *Madong) initmux() {
	mux := mux.NewRouter()
	//Herunder indsættes nye URLs i formattet:

	api := mux.PathPrefix("/api").Subrouter()

	api.HandleFunc("", md.apiOverview).Methods("GET")
	api.HandleFunc("/git", md.httpGitGet).Methods("GET")
	//scanners
	scanners := api.PathPrefix("/scanners").Subrouter()
	scanners.HandleFunc("", md.httpScannersNew).Methods("POST")
	scanners.HandleFunc("/{zone}", md.httpScannersGet).Methods("GET")

	// Crypt
	crypt := api.PathPrefix("/crypt").Subrouter()
	crypt.HandleFunc("/open/{card}", md.authMiddleware(md.httpScannersCryptoOpen)).Methods("GET")
	crypt.HandleFunc("/cipher", md.httpScannersCryptoCipher).Methods("POST")
	crypt.HandleFunc("/verify", md.httpScannersCryptoVerify).Methods("POST")

	// users
	users := api.PathPrefix("/users").Subrouter()
	users.HandleFunc("", md.httpUsers).Methods("GET")
	users.HandleFunc("", md.httpUsersNew).Methods("POST")
	users.HandleFunc("/{card}", md.httpUser).Methods("GET")
	users.HandleFunc("/search/{query}", md.httpUsers).Methods("GET")
	users.HandleFunc("/{card:[0-9A-Fa-f]+}", md.httpUsersDel).Methods("DELETE")
	users.HandleFunc("/{card:[0-9A-Fa-f]+}/roles", md.httpUsersRoles).Methods("GET")
	users.HandleFunc("/{card:[0-9A-Fa-f]+}/roles", md.httpUsersAddRole).Methods("POST")
	users.HandleFunc("/{card:[0-9A-Fa-f]+}/roles", md.httpUsersDelRole).Methods("DELETE")

	users.HandleFunc("/{card:[0-9A-Fa-f]+}/updatepin", md.httpUpdPin).Methods("PATCH")
	users.HandleFunc("/{card:[0-9A-Fa-f]+}/genkey", md.httpUsersGenkey).Methods("GET")

	// roles
	roles := api.PathPrefix("/roles").Subrouter()
	roles.HandleFunc("", md.httpRoles).Methods("GET")
	roles.HandleFunc("", md.httpRolesNew).Methods("POST")
	roles.HandleFunc("/{role}", md.httpRolesUpd).Methods("PATCH")
	roles.HandleFunc("/{role}", md.httpRolesDel).Methods("DELETE")
	roles.HandleFunc("/{role}", md.httpRolesPolicies).Methods("GET")
	roles.HandleFunc("/{role}/users", md.httpRolesUsers).Methods("GET")
	roles.HandleFunc("/{role}/policies", md.httpRolesPoliciesNew).Methods("POST")

	//WebInterface
	//WebInterface := api.PathPrefix("/web").Subrouter()
	//WebInterface.HandleFunc("", md.httpWebHome)
	//WebInterface.HandleFunc("/result", md.httpWebResult)
	// policies

	fileserver := http.FileServer(http.Dir(md.root + "/static"))
	mux.PathPrefix("/").Handler(fileserver)
	policies := api.PathPrefix("/policies").Subrouter()
	policies.HandleFunc("/{policy}", md.httpPoliciesUpd).Methods("PATCH")
	policies.HandleFunc("/{policy}", md.httpPoliciesDel).Methods("DELETE")

	err := mux.Walk(md.gorillaWalkFn)
	if err != nil {
		log.Fatal(err)
	}

	md.mux = mux
}

func (md *Madong) httpGitGet(w http.ResponseWriter, r *http.Request) {

	httpFormatResp(w, md.git, http.StatusOK)
}

//Function to create a new scanner

var linksThatAreGlobalAndCanNeverIntefereWithTheSystem api.LinksOverview

func (md *Madong) apiOverview(w http.ResponseWriter, r *http.Request) {

	httpFormatRespOverview(w, http.StatusOK, linksThatAreGlobalAndCanNeverIntefereWithTheSystem)
}

func (md *Madong) gorillaWalkFn(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
	pathTemplate, err := route.GetPathTemplate()
	if err == nil {
		//fmt.Println("ROUTE:", pathTemplate)
	}
	methodTemp := ""
	methods, err := route.GetMethods()
	if err == nil {
		methodTemp = strings.Join(methods, ",")
	}
	linksThatAreGlobalAndCanNeverIntefereWithTheSystem = append(linksThatAreGlobalAndCanNeverIntefereWithTheSystem, api.LinkOverview{Href: pathTemplate, Method: methodTemp})

	//log.Printf(pathTemplate)

	fmt.Println(linksThatAreGlobalAndCanNeverIntefereWithTheSystem)
	return nil

}

// Return a json string in the format { "err": $err} where $err is the string passed in.
// Code specifies the http code to set, this could be 404 or 500 etc.
func httpError(w http.ResponseWriter, err string, code int) {
	resp := &api.Response{Err: err}

	log.Printf("ERROR: %s", err)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	// We do not check this error
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		s := fmt.Sprintf("Error formatting error response %+v", resp)
		log.Println(s)
		fmt.Fprintf(w, s)
	}
}

// Return a successfull json result in the format { "err": "", "data": $data }, where $data is the data parameter.
// Data can be everything booleans, structs, lists, whatever.
// Code should be something like StatusOK or StatusCreated or something.
// If this function fails to encode the json it will write an error instead
func httpFormatRespWithLinks(w http.ResponseWriter, data interface{}, code int, links api.Links) {
	str, err := json.Marshal(data)

	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp := &api.Response{Data: api.DataType(str), Links: links}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
	}
}

func httpFormatResp(w http.ResponseWriter, data interface{}, code int) {
	httpFormatRespWithLinks(w, data, code, nil)
}

func httpFormatRespOverview(w http.ResponseWriter, code int, linko api.LinksOverview) {
	resp := &api.Response{LinksOverview: linko}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
	}
}

// Look up pubkey and returns the pubkey stored in the database base64 decoded.
// If this is implemented wrong and it returns okay the byte array would still be wrong
func (md *Madong) lookupScannerKey(pubkey string) (bool, []byte, error) {
	fmt.Printf("Lookup %s -> ", pubkey)
	var dbpubkey string

	row := md.db.QueryRow("SELECT publickey FROM scanners WHERE publickey = $1", pubkey)
	if err := row.Scan(&dbpubkey); err != nil {
		// If no rows don't return it as a error
		if err == sql.ErrNoRows {
			fmt.Printf("No rows\n")
			return false, nil, nil
		}
		return false, nil, err
	}

	// Check if the key is correct
	fmt.Printf("found %s -> ", dbpubkey)
	if dbpubkey != pubkey {
		fmt.Printf("No match\n")
		return false, nil, nil
	}
	fmt.Printf("Match\n")

	dbpubkeybin, err := base64.StdEncoding.DecodeString(dbpubkey)
	if err != nil {
		return false, nil, err
	}

	return true, dbpubkeybin, nil
}

func extractAuthHeader(r *http.Request) (*api.AuthToken, string, error) {
	authheaderlist, ok := r.Header["Authorization"]
	if !ok {
		return nil, "", nil
	}

	// Decode header
	authheader := strings.Fields(authheaderlist[0])
	if authheader[0] != api.AuthType || len(authheader) < 2 {
		return nil, "", nil
	}

	at, err := api.DecodeToken(authheader[1])
	if err != nil {
		return nil, "", err
	}

	return at, authheader[1], nil
}

func returnNotAllowed(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", api.AuthType)
	w.WriteHeader(http.StatusForbidden)
}

// TODO is there any way to make this smaller
func (md *Madong) authMiddleware(h func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		now := time.Now()
		at, rawtoken, err := extractAuthHeader(r)
		if err != nil {
			httpError(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if at == nil {
			returnNotAllowed(w)
			return
		}

		okay, pubkey, err := md.lookupScannerKey(at.Publickey)
		if err != nil {
			httpError(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if !okay {
			httpError(w, "Publickey denied", http.StatusForbidden)
			return
		}

		// Check nonce
		nonce := strconv.FormatUint(uint64(at.Nonce), 10)
		fmt.Printf("Nonce: %s\n", nonce)
		_, ok := md.cache.Get(nonce)
		if ok {
			httpError(w, "Repeated nonce", http.StatusForbidden)
			return
		}
		// Save nonce
		md.cache.Set(nonce, nil, cache.DefaultExpiration)

		// Check time
		ts := time.Unix(int64(at.Timestamp), 0)
		fmt.Printf("Comparing %v to now: %v\n", ts, now)
		if ts.Before(now.Add(-1*time.Minute)) || ts.After(now) {
			httpError(w, "Invalid timestamp", http.StatusForbidden)
			return
		}

		// Verify
		okay, err = api.VerifyToken(pubkey, rawtoken)
		if err != nil {
			httpError(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if !okay {
			httpError(w, "Could not verify token", http.StatusForbidden)
			return
		}

		h(w, r, at.Publickey)
	}
}
