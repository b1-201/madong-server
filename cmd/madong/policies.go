package main

import (
	"fmt"

	"github.com/gorilla/mux"
	//"github.com/lib/pq"
	"encoding/json"
	"net/http"
	"time"
	//"database/sql"
)

/*
func (md *Madong) httpPolicies(w http.ResponseWriter, r *http.Request) {
	var policies pq.StringArray

	// Fetch all policies
	err := md.db.Select(&policies, "SELECT id FROM policies")
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write them back
	httpFormatResp(w, &policies, http.StatusOK)
}
*/

// When inserting values in Postman, strings are within " " while values are without.
// Update policy
func (md *Madong) httpPoliciesUpd(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["policy"]
	var newPolicy Policy
	err := json.NewDecoder(r.Body).Decode(&newPolicy)
	if !ok {
		http.Error(w, "Mangler argument", http.StatusBadRequest)
		return
	}
	fmt.Printf("Patching policy %+v\n", newPolicy)

	_, err = md.db.Exec(`UPDATE policies SET zone=$1, tbegin=$2, tend=$3, state=$4 WHERE id = $5`,
		newPolicy.Zone, time.Time(newPolicy.Begin), time.Time(newPolicy.End), newPolicy.State, id)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// Delete policy
func (md *Madong) httpPoliciesDel(w http.ResponseWriter, r *http.Request) {
	id, ok := mux.Vars(r)["policy"]
	if !ok {
		http.Error(w, "Mangler argument", http.StatusBadRequest)
		return
	}

	// Delete from database
	_, err := md.db.Exec("DELETE FROM policies WHERE id = $1", id)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
