package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	. "gitlab.com/b1-201/madong-server/pkg/api"
)

// Get all roles in the database and return them as a string
func (md *Madong) httpRoles(w http.ResponseWriter, r *http.Request) {
	// StringArray is the same as []string, but works better when extracting from database
	links := Links{
		Link{Href: "/roles", Method: "POST"},
		Link{Href: "/roles/{role}", Method: "GET"},
		Link{Href: "/roles/{role}", Method: "PATCH"},
		Link{Href: "/roles/{role}", Method: "DELETE"},
		Link{Href: "/users/{role}/users", Method: "GET"},
		Link{Href: "/users/{role}/policies", Method: "POST"},
	}

	var roles []Role

	// Fetch all roles
	err := md.db.Select(&roles, "SELECT name FROM roles")
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write them back as json and a statusOk code 200
	httpFormatRespWithLinks(w, &roles, http.StatusOK, links)
}

//Add role
func (md *Madong) httpRolesNew(w http.ResponseWriter, r *http.Request) {
	var role Role
	err := json.NewDecoder(r.Body).Decode(&role)

	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = md.db.NamedExec(`INSERT INTO roles (name)
	VALUES (:name)`, role)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

//Delete role
func (md *Madong) httpRolesDel(w http.ResponseWriter, r *http.Request) {
	role, ok := mux.Vars(r)["role"]
	if !ok {
		http.Error(w, "Invalid role argument", http.StatusBadRequest)
		return
	}

	// Delete from database
	_, err := md.db.Exec(`DELETE FROM roles WHERE name = $1`, role)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

//Update role
func (md *Madong) httpRolesUpd(w http.ResponseWriter, r *http.Request) {
	role, ok := mux.Vars(r)["role"]
	var newRole Role
	err := json.NewDecoder(r.Body).Decode(&newRole)

	if !ok {
		http.Error(w, "Invalid role argument", http.StatusBadRequest)
		return
	}

	// Update role name
	_, err = md.db.Exec(`UPDATE roles SET name=$1 WHERE name = $2`, newRole.Name, role)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// This gets all the policies of a specific role, by joining policies and roles
const selectRulePolicies = `
SELECT policies.id, zone, tbegin, tend, state FROM policies
INNER JOIN roles ON roles.id = policies.role
WHERE roles.name = $1
`

// Get all the policies for at specific role, using the format /roles/student
func (md *Madong) httpRolesPolicies(w http.ResponseWriter, r *http.Request) {
	var policies []Policy
	links := Links{
		Link{Href: "{role}/users", Method: "GET"},
		Link{Href: "{role}/policies", Method: "POST"},
	}
	// Fetch the role name
	role, ok := mux.Vars(r)["role"]
	if !ok {
		httpError(w, "missing parameter role", http.StatusBadRequest)
		return
	}

	// Query the databse using the selectRulePolicies
	if err := md.db.Select(&policies, selectRulePolicies, role); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// Return a 404 if we found nothing
	if len(policies) == 0 {
		httpError(w, "role has no policies", http.StatusNotFound)
		return
	}

	httpFormatRespWithLinks(w, policies, http.StatusOK, links)

}

const createPolicy = `
INSERT INTO policies (role, zone, tbegin, tend, state)
VALUES ($1, $2, $3, $4, $5)
`

func (md *Madong) httpRolesPoliciesNew(w http.ResponseWriter, r *http.Request) {
	var newPolicy Policy

	role, ok := mux.Vars(r)["role"]
	if !ok {
		httpError(w, "missing parameter role", http.StatusBadRequest)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&newPolicy)
	if !ok {
		http.Error(w, "Mangler argument", http.StatusBadRequest)
		return
	}
	fmt.Printf("Patching policy %+v\n", newPolicy)

	var roleid string
	md.db.QueryRow("SELECT id FROM roles WHERE name=$1", role).Scan(&roleid)

	_, err = md.db.Exec(createPolicy,
		roleid, newPolicy.Zone, time.Time(newPolicy.Begin), time.Time(newPolicy.End), newPolicy.State)
	if err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

// Select all users in a specific role, using the middle roles_users
const selectRoleUsers = `
SELECT users.id, users.name FROM users
INNER JOIN roles_users ON users.id = roles_users.user_id
INNER JOIN roles ON roles.id = roles_users.role_id
WHERE roles.name = $1`

// Select all users for a specific role using the string defined above
func (md *Madong) httpRolesUsers(w http.ResponseWriter, r *http.Request) {
	var cards []Card

	role, ok := mux.Vars(r)["role"]
	if !ok {
		httpError(w, "missing parameter role", http.StatusBadRequest)
		return
	}

	if err := md.db.Select(&cards, selectRoleUsers, role); err != nil {
		httpError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// Return 404 is empty
	if len(cards) == 0 {
		httpError(w, "role not found", http.StatusNotFound)
		return
	}

	// Return result
	httpFormatResp(w, cards, http.StatusOK)
}
