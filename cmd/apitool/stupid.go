package main

import (
	"crypto/ed25519"
	"crypto/rand"
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/b1-201/madong-server/pkg/api"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("usage: apitool <command> [<args>]")
		fmt.Println("Available subcommands: ")
		fmt.Println(" genkey     Generates a new keyset")
		fmt.Println(" gentoken   Generates a new token with the specified key")
		return
	}

	switch os.Args[1] {
	case "genkey":
		genkey()
	case "gentoken":
		gentoken(os.Args[2:])
	default:
		fmt.Printf("%s is not a valid command.\n", os.Args[1])
	}
}

func genkey() {
	// Generate key
	pubkey, privkey, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("pubkey: %s \nprivkey: %v\n",
		base64.StdEncoding.EncodeToString(pubkey),
		base64.StdEncoding.EncodeToString(privkey),
	)
}

func gentoken(args []string) {
	flags := flag.NewFlagSet("gentoken", flag.ExitOnError)

	privkeystr := flags.String("privkey", "", "Private key to generate token with")
	pubkeystr := flags.String("pubkey", "", "Public key to include in token")

	flags.Parse(args)

	privkey, err := base64.StdEncoding.DecodeString(*privkeystr)
	if err != nil {
		log.Fatal(err)
	}

	at, err := api.GenAuthToken(*pubkeystr, rand.Reader)
	if err != nil {
		log.Fatal(err)
	}
	token := at.Sign(privkey)
	fmt.Printf("at: %+v\ntoken: %s\n", at, token)
}
