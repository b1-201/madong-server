# madong-server

## How to set up local dev environment

`make p single` / `make clean`

- API on localhost port 8181
- Webinterface for postgres on localhost port 8080

### Requirements

- docker
- docker-compose
- make
- entr
- go

# Typical workflow

0. Write test for new feature in app
1. Implement feature in `src/`
2. `make p single`
3. Run test
4. `make p clean`
5. Repeat step 2 through 4 until feature works
6. Add test to relevant file (usually `*.go` or `tests.sh`)

Changes can be applied everytime you save with e.g.

`make p watch`


# Note til Nicholas
- Kør serveren med ./src under i src-mappen.
- Ellers go build i madong-server, og prøv igen.

