FROM golang:alpine

RUN apk add --no-cache git

EXPOSE 8181

WORKDIR $GOPATH

RUN go get github.com/gorilla/mux
RUN go get github.com/jmoiron/sqlx
RUN go get github.com/lib/pq
RUN go get github.com/pkg/errors
RUN go get github.com/patrickmn/go-cache
RUN go get gitlab.com/b1-201/gofare
RUN go get golang.org/x/crypto/bcrypt

ENV MADONG_ROOT=$GOPATH/src/gitlab.com/b1-201/madong-server/

WORKDIR $GOPATH/src/gitlab.com/b1-201/madong-server
COPY . .

WORKDIR $GOPATH
RUN go install gitlab.com/b1-201/madong-server/cmd/madong

CMD ["madong"]
